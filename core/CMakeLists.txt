cmake_minimum_required(VERSION 3.17)
set(CMAKE_CXX_STANDARD 20)

set(CORE_SOURCES src/alu.cpp src/bus.cpp src/cpu.cpp src/ram.cpp)
add_library(CORE_LIB STATIC ${CORE_SOURCES})

add_executable(CPUemulator main.cpp)
target_link_libraries(CPUemulator CORE_LIB HELPER_LIB)