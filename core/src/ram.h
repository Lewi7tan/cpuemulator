//
// Created by Artur Twardzik on 12/29/2020.
//

#ifndef CPUEMULATOR_RAM_H
#define CPUEMULATOR_RAM_H

#include <cstdint>
#include <map>
#include <vector>

#include "../../lib/debug.h"

namespace ram {
    class RAM {
    private:
        std::map<uint32_t, uint8_t> memory;

    public:
        inline bool address_already_exist(const uint32_t &address) const {
            if (memory.count(address) != 0) return true;
            return false;
        }

        uint32_t stack_head{};

        uint32_t memory_size{};

    public:
        //! Allocates new variable at given address.
        //! \param address Address where to allocate
        //! \param number_of_bytes Number of bytes to allocate. Due to the fact
        //! that CPU is 32 bit architecture, up to 4 bytes are allowed to store as single value.
        //! \param value Bytes to insert at given location.
        void allocate_value(const uint32_t &address, const uint8_t &number_of_bytes, const uint32_t &value = 0x00);

        //! Assigns new value to allocated address.
        //! \param address Address where does variable exist.
        //! \param number_of_bytes Number of bytes to write. Due to the fact
        //! that CPU is 32 bit architecture, up to 4 bytes are allowed to store as single value.
        //! \param value Bytes to insert at given location.
        void assign_value(const uint32_t &address, const uint8_t &number_of_bytes, const uint32_t &value = 0x00);

        //! Deallocates specified number of bytes starting at given location.
        //! \param address Address begin.
        //! \param number_of_bytes Number of bytes to deallocate.
        void free(const uint32_t &address, const uint8_t &number_of_bytes) noexcept;

        //! Push new value to the stack (starting from highest address).
        //! \param value Value to push. In 32 bit architecture, elements
        //! can be only 16 or 32 bit wide.
        void push(const int32_t &value);

        //! Pop element from the stack.
        //! \return Deleted element.
        int32_t pop();

        //! checks whether RAM has got the same stack pointer as CPU
        //! If not, depending on operation, stack head is moved, and local variables are freed or reserved.
        //! \param stack_pointer ESP register.
        void check_stack(const uint32_t &stack_pointer);

        //! Loads specified program to memory starting from lowest address.
        //! \param instructions Vector of bytes to load.
        void load_program(const std::vector<uint8_t> &instructions);

        //! Reads specified number of bytes starting from given address.
        //! \param address Address begin.
        //! \param number_of_bytes Number of bytes to read.
        //! \return Up to 4 bytes as single value.
        [[nodiscard]] int32_t get_value(const uint32_t &address, const uint8_t &number_of_bytes) const;

        const std::map<uint32_t, uint8_t> &get_memory() const { return this->memory; }

    };
}

#endif //CPUEMULATOR_RAM_H
