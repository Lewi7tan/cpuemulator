//
// Created by Artur Twardzik on 1/25/2021.
//

#include "assembler.h"

Assembler::Assembler(std::string filename, const std::string &output_file, bool debug)
        : main_file(std::move(filename)), debug_mode(debug) {
    auto last_slash = main_file.find_last_of('/');

    if (last_slash != std::string::npos) {
        working_directory = main_file.substr(0, last_slash);
    }

    labels.emplace_back("MAIN", main_file);

    if (!output_file.empty()) executable = output_file;
    else executable = main_file.substr(0, main_file.find('.')) + ".ce";
}

void Assembler::load_file(const std::string &filename) {
    std::fstream file;
    file.open(filename, std::ios::in);
    if (!file.is_open()) throw std::runtime_error("[!] Could not open the file\n");

    section where = TEXT;
    uint32_t line_number{};
    while (file) {
        std::string line{};
        std::getline(file, line);
        line_number++;
        this->total_lines++;
        prepare_line(line);
        if (line.empty()) continue;


        if (line.starts_with("#INCLUDE")) {
            tokenize_include(line);
            continue;
        }
        else if (line.starts_with("SECTION")) {
            if (line.ends_with(".TEXT")) where = TEXT;
            else if (line.ends_with(".DATA")) where = DATA;
            else if (line.ends_with(".BSS")) where = BSS;
            else {
                auto message = "Undefined section: " + line;
                throw std::logic_error(message);
            }

            continue;
        }


        if (where == TEXT) {
            auto parent_namespace = labels.back().get_label_name();

            if (line.starts_with('.') && line.ends_with(':')) {
                auto pair = std::make_pair(
                        parent_namespace,
                        Label(line.substr(0, line.length() - 1), filename, line_number)
                );

                sublabels.insert(pair);
            }
            else if (line.ends_with(':')) {
                labels.emplace_back(line.substr(0, line.length() - 1), filename, line_number);
            }
            else {
                Instruction current;
                current.occur_line = line_number;
                current.parent_namespace = parent_namespace;
                current.occur_file = filename;

                tokenize(line, current);

                instructions.push_back(current);
            }
        }
        else if (where == DATA) {
            tokenize_data(line);
        }
        else if (where == BSS) {
            tokenize_bss(line);
        }
    }

    file.close();
}

void Assembler::prepare_line(std::string &line) {
    int comment_position = line.find(';');
    if (comment_position != std::string::npos) line = line.substr(0, comment_position);

    if (!line.empty()) {
        bool string_literal = false;
        for (auto &it : line) {
            if (it == '"' || it == '<' || it == '>') {
                string_literal = !string_literal;
                continue;
            }

            if (!string_literal) it = std::toupper(it);
        }


        auto begin = line.find_first_not_of(" \t");
        auto end = line.find_last_not_of(" \t") + 1;
        line = line.substr(0, end);
        line = line.substr(begin);
    }
}

void Assembler::tokenize(const std::string &line, Instruction &instruction) {
    auto comma_pos = line.find(',');
    std::stringstream tok(line);

    tok >> instruction.keyword;

    // ---- LEFT PARAMETER -----
    std::string temp_left{};
    tok >> temp_left;

    if (line.find(',') == std::string::npos) {
        instruction.left = temp_left;
    }
    else {
        if (temp_left.ends_with(',')) {
            auto last = temp_left.find_last_not_of(" ,\t") + 1;
            instruction.left = temp_left.substr(0, last);
        }
        else {
            auto ptr_size_iterator = line.find(temp_left);

            while (line.at(ptr_size_iterator) != '[') {
                if (!std::isspace(line.at(ptr_size_iterator))) {
                    instruction.left_ptr_size += line.at(ptr_size_iterator);
                }
                ++ptr_size_iterator;
            }

            auto left_length = comma_pos - ptr_size_iterator;
            instruction.left = line.substr(ptr_size_iterator, left_length);
        }

        // ---- RIGHT PARAMETER ----

        std::string temp_right = line.substr(comma_pos);
        auto begin = temp_right.find_first_not_of(" ,\t");
        temp_right = temp_right.substr(begin);

        auto it = temp_right.rbegin();
        size_t end = 0;
        while (std::isspace(*it)) {
            ++it;
            ++end;
        }
        temp_right = temp_right.substr(0, temp_right.length() - end);


        if (temp_right.find('[') != std::string::npos) {
            auto ptr_size_iterator = line.find(temp_right);

            while (line.at(ptr_size_iterator) != '[') {
                if (!std::isspace(line.at(ptr_size_iterator))) {
                    instruction.right_ptr_size += line.at(ptr_size_iterator);
                }
                ++ptr_size_iterator;
            }

            instruction.right = line.substr(ptr_size_iterator);

        }
        else {
            instruction.right = temp_right;
        }
    }
}

void Assembler::tokenize_data(std::string line) {
    //[variable-name]    define-directive    initial-value   [,initial-value]...
    // (int) e.g. variable dd 16
    // (array of ints) e.g. variable dd 10, 20, 30, 40
    // (array of bytes) e.g. variable times 8 db

    while (contains_string_literal(line)) {
        auto start = line.find('"');
        auto stop = start + 1;

        std::string string_literal{};
        while (line.at(stop) != '"') {
            string_literal += line.at(stop);
            ++stop;
        }

        std::string converted_literal = string2hex(string_literal);

        line.replace(start, string_literal.length() + 2, converted_literal);

    }

    std::stringstream tok(line);
    Variable variable;

    std::string define_directive;
    int times{};
    int cell_size{};

    tok >> variable.name;
    tok >> define_directive;

    if (define_directive == "TIMES") {
        std::string number;
        tok >> number;
        times = std::stoi(number);

        define_directive.clear();
        tok >> define_directive;
    }

    if (define_directive == "DB") cell_size = 1;
    else if (define_directive == "DW") cell_size = 2;
    else if (define_directive == "DD") cell_size = 4;
    else {
        auto message = "Not recognised size: " + define_directive + "\n\t" + line;
        throw std::logic_error(message);
    }

    while (tok) {
        std::string temp;
        tok >> temp;

        if (temp.ends_with(',')) temp = temp.substr(0, temp.length() - 1);

        if (!temp.empty()) {
            if (!temp.ends_with('H')) temp = dec2hex(temp);
            else temp = temp.substr(0, temp.length() - 1);
            while (temp.length() != cell_size * 2) temp.insert(0, "0");

            variable.bytes += temp;
        }
    }

    if (times) {
        auto number_of_bytes = times * cell_size;

        while (variable.bytes.length() != number_of_bytes * 2) variable.bytes += "0";
    }

    variables.push_back(variable);
}

void Assembler::tokenize_bss(const std::string &line) {
    //[variable-name] define-directive size
    std::stringstream tok(line);
    Variable variable;
    tok >> variable.name;

    std::string define_directive;
    tok >> define_directive;

    int size{};
    tok >> size;

    if (define_directive == "RESB") size = size * 1;
    else if (define_directive == "RESW") size = size * 2;
    else if (define_directive == "RESD") size = size * 4;
    else {
        auto message = "Not recognised size: " + define_directive + "\n\t" + line;
        throw std::logic_error(message);
    }

    while (variable.bytes.length() != size * 2) variable.bytes += "00";

    variables.push_back(variable);
}

void Assembler::tokenize_include(const std::string &line) {
    auto pos = line.find('<') + 1;
    if (pos == std::string::npos) throw std::logic_error("Not found filename of file to include.");

    std::string filename{};
    auto it = std::next(line.begin(), pos);
    while (*it != '>') {
        filename += *it;
        ++it;
    }

    auto include_file = (!working_directory.empty() ? working_directory + '/' : "") + filename;
    external_includes.push_back(include_file);
}

void Assembler::append_includes() {
    for (const auto &file : external_includes) {
        load_file(file);
    }
}

void Assembler::run() {
    load_file(main_file);
    append_includes();

    for (auto &instruction : instructions) {
        machine_codes.push_back(transform_to_opcodes(instruction));
    }
    update_labels_addresses();
    update_variables_addresses();

    if (debug_mode) save_debug();
    else save_release();
}

void Assembler::save_release() {
    std::fstream file;
    file.open(executable, std::ios::out);

    for (const auto &line : machine_codes) file << line << "\n";
    for (const auto &variable : variables) file << variable.bytes << "\n";

    file.close();
}

void Assembler::save_debug() {
    auto generate_spaces = [](const std::string &machine_code) {
        uint8_t length = 43 - machine_code.length();

        std::string spaces{};
        for (size_t i = 0; i < length; ++i) spaces += " ";

        return spaces;
    };

    std::fstream file;
    file.open(executable, std::ios::out);

    auto machine_code = machine_codes.begin();
    auto instruction = instructions.begin();

    std::string current_parent_namespace = instruction->parent_namespace;
    file << ";--fn--:" << current_parent_namespace << "\n";

    while (machine_code != machine_codes.end()) {
        if (instruction->parent_namespace != current_parent_namespace) {
            current_parent_namespace = instruction->parent_namespace;
            file << ";--fn--:" << instruction->parent_namespace << "\n";
        }

        file << *machine_code
             << generate_spaces(*machine_code) << ";"
             << instruction->get_instruction() << "\n";

        ++machine_code;
        ++instruction;
    }

    file << "\n;--VARIABLES--\n";
    for (const auto &variable : variables) file << variable.bytes << "\t;" << variable.name << "\n";

    file.close();
}

std::string Assembler::transform_to_opcodes(Instruction &current) {
    //as a small convenience at the end of this function every number
    //in a vector representing current current is converted to hexadecimal
    std::vector<uint8_t> ret;

    current.instruction_address = this->address;
    current_executed_line = current.occur_line;
    current_executed_file = current.occur_file;

    if (current.keyword == "NOP") ret.push_back(0x00);
    else if (current.keyword == "POP" && current.left.empty()) ret.push_back(0x01);
    else if (current.keyword == "RET") {
        if (is_register(current.left)) {
            ret = handle_reg(0x0A, current.left);
        }
        else if (!current.left.empty()) {
            ret.push_back(0x20);
            auto numbers = handle_dec_hex(current.left);
            ret.insert(ret.end(), numbers.begin(), numbers.end());
        }
        else ret.push_back(0x02);
    }
    else if (current.keyword == "INC") ret = handle_reg(0x05, current.left);
    else if (current.keyword == "DEC") ret = handle_reg(0x06, current.left);
    else if (current.keyword == "POP") ret = handle_reg(0x07, current.left);
    else if (current.keyword == "PUSH") {
        if (is_register(current.left)) {
            ret = handle_reg(0x08, current.left);
        }
        else if (is_number(current.left)) {
            ret.push_back(0x19);
            auto numbers = handle_dec_hex(current.left);
            ret.insert(ret.end(), numbers.begin(), numbers.end());
        }
        else {
            ret = handle_variable(0x19, current.left);
        }
    }
    else if (current.keyword == "NOT") ret = handle_reg(0x09, current.left);
    else if (current.keyword == "INT") {
        ret.push_back(0x10);
        auto interrupt_number = std::stoi(current.left, nullptr, 16);
        ret.push_back(interrupt_number);
    }
    else if (current.keyword == "CALL") ret = handle_jump(0x11, current);
    else if (current.keyword == "JMP") ret = handle_jump(0x12, current);
    else if ((current.keyword == "JE") || (current.keyword == "JZ")) ret = handle_jump(0x13, current);
    else if (current.keyword == "JG") ret = handle_jump(0x14, current);
    else if (current.keyword == "JGE") ret = handle_jump(0x15, current);
    else if (current.keyword == "JL") ret = handle_jump(0x16, current);
    else if (current.keyword == "JLE") ret = handle_jump(0x17, current);
    else if ((current.keyword == "JNE") || (current.keyword == "JNZ")) ret = handle_jump(0x18, current);
    else if (current.keyword == "MOV") {
        if (!current.left_ptr_size.empty()) {
            ret = handle_left_size_ptr(current);
        }
        else if (!current.right_ptr_size.empty()) {
            ret = handle_right_size_ptr(current);
        }
        else if (is_register(current.left) && is_register(current.right)) {
            ret = handle_reg(0x21, current.left, current.right);
        }
        else if (is_register(current.left) && is_number(current.right)) {
            ret = handle_reg_value(0x31, current.left, current.right);
        }
        else {
            ret = handle_variable(0x31, current.right, current.left);
        }
    }
    else if (current.keyword == "CMP") {
        if (is_register(current.left) && is_register(current.right)) {
            ret = handle_reg(0x22, current.left, current.right);
        }
        else if (is_register(current.left)) {
            ret = handle_reg(0x32, current.left);

            auto value = handle_dec_hex(current.right);
            ret.insert(ret.end(), value.begin(), value.end());
        }
        else {
            ret.push_back(0x52);

            auto left = handle_dec_hex(current.left);
            auto right = handle_dec_hex(current.right);

            ret.insert(ret.end(), left.begin(), left.end());
            ret.insert(ret.end(), right.begin(), right.end());
        }
    }
    else if (current.keyword == "MUL") ret = handle_math(0x23, current);
    else if (current.keyword == "DIV") ret = handle_math(0x24, current);
    else if (current.keyword == "ADD") ret = handle_math(0x25, current);
    else if (current.keyword == "SUB") ret = handle_math(0x26, current);
    else if (current.keyword == "XOR") ret = handle_math(0x27, current);
    else if (current.keyword == "AND") ret = handle_math(0x28, current);
    else if (current.keyword == "OR") ret = handle_math(0x29, current);
    else if (current.keyword == "TEST") ret = handle_math(0x2A, current);
    else if (current.keyword == "SHL") ret = handle_math(0x61, current);
    else if (current.keyword == "SHR") ret = handle_math(0x62, current);

    else {
        if (!current.keyword.empty()) {
            std::string error_message = generate_error_message("Instruction: % not recognised.");

            throw std::logic_error(debug::format_exception_message(error_message, current.keyword));
        }
    }


    address += ret.size();

    std::string opcodes;
    std::for_each(ret.begin(), ret.end(), [&](const uint32_t &number) {
        std::stringstream str;
        str << std::hex << std::uppercase;
        if (number < 16) str << 0;
        str << number;

        opcodes += str.str();
    });
    return opcodes;
}

void Assembler::update_labels_addresses() {
    for (auto &label : labels) {
        assign_address_to_label(label);

        auto label_name = label.get_label_name();
        for (auto it = instructions.begin(); it != instructions.end(); ++it) {
            if (it->left == label_name) {
                auto where = std::distance(instructions.begin(), it);

                auto &element = machine_codes.at(where);
                std::string opcode = element.substr(0, 2);

                element.clear();
                element = opcode + label.get_address();
            }
        }
    }

    for (auto &sublabel : sublabels) {
        assign_address_to_label(sublabel.second);

        auto parent_namespace = sublabel.first;
        auto start_instruction_index = std::find_if(instructions.begin(), instructions.end(),
                                                    [parent_namespace](const Instruction &instruction) {
                                                        if (instruction.parent_namespace == parent_namespace) {
                                                            return true;
                                                        }
                                                        return false;
                                                    });

        auto it = start_instruction_index;
        while (it->parent_namespace == parent_namespace) {
            if (it->left == sublabel.second.get_label_name()) {
                auto where = std::distance(instructions.begin(), it);

                auto &element = machine_codes.at(where);
                std::string opcode = element.substr(0, 2);

                element.clear();
                element = opcode + sublabel.second.get_address();
            }

            ++it;
            if (it == instructions.end()) break;
        }
    }
}

void Assembler::assign_address_to_label(Label &label) {
    auto line = label.get_occur_line() + 1;
    auto file = label.get_occur_file();
    for (auto &instruction : instructions) {
        if (instruction.occur_line == line && instruction.occur_file == file) {
            label.set_address(instruction.instruction_address);
            break;
        }
    }
}

void Assembler::update_variables_addresses() {
    // TODO: optimise it; the complexity of this algorithm is O(n*m),
    //  where n is number of variables declared, and m is number of lines in text section.

    for (auto &variable : variables) {
        variable.address = this->address;
        this->address += variable.bytes.length() / 2;

        auto current_var_address = dec2hex(std::to_string(variable.address));
        while (current_var_address.length() < 8) current_var_address.insert(0, "0");


        for (auto instruction = instructions.begin(); instruction != instructions.end(); ++instruction) {
            auto where = std::distance(instructions.begin(), instruction);
            auto &machine_code = machine_codes.at(where);

            auto left = prepare_addressing(instruction->left);
            auto right = prepare_addressing(instruction->right);

            int half_byte = 0; //number of byte divided by two - one character in string representation

            //zeroes are placeholders for variables' addresses
            if (left == variable.name) half_byte = machine_code.find("00000000");
            else if (right == variable.name) half_byte = machine_code.rfind("00000000");
            else continue;

            change_machine_code(machine_code, current_var_address, half_byte);
        }
    }
}

std::string Assembler::prepare_addressing(std::string line) {
    if (line.starts_with('[')) {
        line = line.substr(1, line.length() - 2);

        auto indirect_pos = line.find_first_of("+-");
        if (indirect_pos) line = line.substr(0, indirect_pos);
    }

    return line;
}

void Assembler::change_machine_code(std::string &instruction, const std::string &value, uint8_t first_half_byte) {
    auto first = instruction.substr(0, first_half_byte);

    auto last_byte = first_half_byte + 8;
    auto second = instruction.substr(last_byte);

    instruction = first + value + second;
}

std::string Assembler::dec2hex(const std::string &number) const {
    int32_t byte = std::stoi(number);
    std::stringstream str;
    str << std::hex << std::uppercase << byte;
    return str.str();
}

std::string Assembler::string2hex(const std::string &text) {
    std::stringstream hex;

    for (auto letter : text) {
        uint32_t num = letter;

        hex << std::hex << std::uppercase << num << "H, ";
    }

    return hex.str();
}

uint8_t Assembler::transform_register_to_num(const std::string &reg) {
    uint8_t r{};
    if (reg == "ESP") r = 0x10;
    else if (reg == "EBP") r = 0x11;
    else if (reg == "ESI") r = 0x12;
    else if (reg == "EDI") r = 0x13;
    else if (reg == "EIP") r = 0x14;
    else {
        std::string gp{};
        gp = reg.substr(1, reg.back());
        for (char &it : gp) {
            if (!std::isdigit(it)) {
                throw std::logic_error(generate_error_message("Register number has to be in range 0 - 15"));
            }
        }

        uint8_t reg_num = std::stoi(gp);
        if (reg_num < 16) r = reg_num;
        else throw std::logic_error(generate_error_message("Register number has to be in range 0 - 15"));
    }

    return r;
}

std::vector<uint8_t> Assembler::handle_reg(const uint8_t &opcode,
                                           const std::string &reg_l_number,
                                           const std::string &reg_r_number) {

    std::vector<uint8_t> machine_code;

    machine_code.push_back(opcode);
    machine_code.push_back(transform_register_to_num(reg_l_number));
    if (!reg_r_number.empty()) machine_code.push_back(transform_register_to_num(reg_r_number));

    return machine_code;
}

std::vector<uint8_t> Assembler::handle_reg_value(const uint8_t &opcode,
                                                 const std::string &reg,
                                                 const std::string &value) {
    std::vector<uint8_t> machine_code{opcode};
    machine_code.push_back(transform_register_to_num(reg));

    auto number = handle_dec_hex(value);
    machine_code.insert(machine_code.end(), number.begin(), number.end());

    return machine_code;
}

std::vector<uint8_t> Assembler::handle_jump(const uint8_t &opcode, const Instruction &instruction) {
    std::vector<uint8_t> machine_code;

    machine_code.push_back(opcode);
    auto global_function_address = std::find_if(labels.begin(),
                                                labels.end(),
                                                [instruction](const Label &other) {
                                                    return (other.get_label_name() == instruction.left);
                                                });

    bool local_function_address = false;
    auto parent = sublabels.equal_range(instruction.parent_namespace);
    for (auto it = parent.first; it != parent.second; ++it) {
        if (it->second.get_label_name() == instruction.left) local_function_address = true;
    }


    if (global_function_address != std::end(labels) || local_function_address) {
        machine_code.insert(machine_code.end(), {0, 0, 0, 0});
    }
    else {
        std::string error_message = generate_error_message("Label: % does not exist.");

        throw std::logic_error(debug::format_exception_message(error_message, instruction.left));
    }

    return machine_code;
}

std::vector<uint8_t> Assembler::handle_math(const uint8_t &opcode, const Instruction &instruction) {
    std::vector<uint8_t> machine_code;

    if (is_register(instruction.right)) machine_code = handle_reg(opcode, instruction.left, instruction.right);
    else machine_code = handle_reg_value(opcode + 0x10, instruction.left, instruction.right);

    return machine_code;
}

std::vector<uint8_t> Assembler::handle_dec_hex(const std::string &number) {
    std::vector<uint8_t> machine_code;
    std::string temp{};

    auto it_end = --number.end();
    if (!number.ends_with('H')) temp = dec2hex(number);
    else std::copy(number.begin(), it_end, std::back_inserter(temp));

    while (temp.length() < 8) {
        temp.insert(0, "0");
    }

    for (size_t i = 0; i < temp.length(); i += 2) {
        std::string byte = temp.substr(i, 2);
        machine_code.push_back(std::stoi(byte, nullptr, 16));
    }

    return machine_code;
}

std::vector<uint8_t> Assembler::convert_from_indirect(const std::string &indirect_address) {
    std::vector<uint8_t> machine_code;

    std::string left{}; //before '+/-' sign
    std::string right{}; //after '+/-' sign
    std::string operation{}; //'+/-' sign
    bool after_operation = false;
    for (auto indirect_addres : indirect_address) {
        if (indirect_addres == '+' || indirect_addres == '-') {
            operation = indirect_addres;
            after_operation = true;
            continue;
        }

        if (!after_operation && !std::isspace(indirect_addres)) left += indirect_addres;
        else {
            if (!std::isspace(indirect_addres)) right += indirect_addres;
        }
    }


    //move address to esi
    if (is_register(left)) {
        machine_code = handle_reg(0x21, "ESI", left);
    }
    else if (variable_exist(left)) {
        //zeroes are placeholders for variables' addresses
        machine_code.insert(machine_code.end(), {0x31, 0x12, 0x00, 0x00, 0x00, 0x00});
    }
    else {
        throw std::logic_error(generate_error_message("Invalid operands by indirect addressing."));
    }


    std::vector<uint8_t> add_or_sub;
    if (is_register(right)) {
        if (operation == "+") add_or_sub = handle_reg(0x25, "ESI", right);
        else add_or_sub = handle_reg(0x26, "ESI", right);
    }
    else if (is_number(right)) {
        if (operation == "+") add_or_sub = handle_reg_value(0x35, "ESI", right);
        else add_or_sub = handle_reg_value(0x36, "ESI", right);
    }

    machine_code.insert(machine_code.end(), add_or_sub.begin(), add_or_sub.end());

    return machine_code;
}

std::vector<uint8_t> Assembler::handle_left_size_ptr(Instruction instruction) {
    std::vector<uint8_t> machine_code;

    if (variable_exist(instruction.left)) {
        //set address temporary to 0x00. It is updated after translating TEXT section
        instruction.left = "00000000H";
    }

    auto param_left = (instruction.left.starts_with('[')
                       ? instruction.left.substr(1, instruction.left.length() - 2)
                       : instruction.left);

    bool indirect = is_indirect_addressing(param_left);
    if (indirect) {
        machine_code = {0x08, 0x12}; //+ PUSH ESI

        auto addr = convert_from_indirect(param_left);
        machine_code.insert(machine_code.end(), addr.begin(), addr.end());

        param_left = "ESI";
    }

    ///////////////////////////////////////////////////////////////////
    auto reg_reg = [&](const uint8_t &opcode) {
        auto where = handle_reg(opcode, param_left, instruction.right);
        machine_code.insert(machine_code.end(), where.begin(), where.end());
    };

    auto reg_value = [&](const uint8_t &opcode) {
        auto where = handle_reg_value(opcode, param_left, instruction.right);
        machine_code.insert(machine_code.end(), where.begin(), where.end());
    };

    auto address_reg = [&](const uint8_t &opcode) {
        machine_code.push_back(opcode);

        auto where = handle_dec_hex(param_left);
        machine_code.insert(machine_code.end(), where.begin(), where.end());

        machine_code.push_back(transform_register_to_num(instruction.right));
    };

    auto address_value = [&](const uint8_t &opcode) {
        machine_code.push_back(opcode);

        auto where = handle_dec_hex(param_left);
        machine_code.insert(machine_code.end(), where.begin(), where.end());

        auto value = handle_dec_hex(instruction.right);
        machine_code.insert(machine_code.end(), value.begin(), value.end());
    };

    auto handle = [&](const uint8_t &r_r, const uint8_t &r_v, const uint8_t &a_r, const uint8_t &a_v) {
        if (is_register(param_left) && is_register(instruction.right)) reg_reg(r_r);
        else if (is_register(param_left) && is_number(instruction.right)) reg_value(r_v);
        else if (is_number(param_left) && is_register(instruction.right)) address_reg(a_r);
        else if (is_number(param_left) && is_number(instruction.right)) address_value(a_v);
        else throw std::logic_error(generate_error_message("Incompatible parameters."));
    };

    ///////////////////////////////////////////////////////////////////
    if (instruction.left_ptr_size == "BYTE") {
        handle(0x2E, 0x3E, 0x43, 0x53);
    }
    else if (instruction.left_ptr_size == "WORD") {
        handle(0x2F, 0x3F, 0x44, 0x54);
    }
    else if (instruction.left_ptr_size == "DWORD") {
        handle(0x30, 0x40, 0x45, 0x55);
    }
    else {
        std::string error_message = generate_error_message("Size % not recognised.");
        throw std::logic_error(debug::format_exception_message(error_message, instruction.left_ptr_size));
    }

    if (indirect) {
        machine_code.insert(machine_code.end(), {0x07, 0x12});  // + POP ESI
    }

    return machine_code;
}

std::vector<uint8_t> Assembler::handle_right_size_ptr(const Instruction &instruction) {
    std::vector<uint8_t> machine_code;
    auto param_right = instruction.right.substr(1, instruction.right.length() - 2);

    bool indirect = is_indirect_addressing(param_right);
    if (indirect) {
        //using ESI is used in order to handle arithmetic operations
        machine_code = {0x08, 0x12}; //+ PUSH ESI

        auto addr = convert_from_indirect(param_right);
        machine_code.insert(machine_code.end(), addr.begin(), addr.end());

        param_right = "ESI";
    }

    ///////////////////////////////////////////
    auto handle = [&](const uint8_t &reg, const uint8_t &reg_addr) {
        if (is_register(param_right)) {
            auto where = handle_reg(reg, instruction.left, param_right);
            machine_code.insert(machine_code.end(), where.begin(), where.end());
        }
        else if (is_number(param_right)) {
            auto where = handle_reg(reg_addr, instruction.left);
            machine_code.insert(machine_code.end(), where.begin(), where.end());

            where = handle_dec_hex(param_right);
            machine_code.insert(machine_code.end(), where.begin(), where.end());
        }
        else if (variable_exist(param_right)) {
            auto where = handle_variable(reg_addr, param_right, instruction.left);
            machine_code.insert(machine_code.end(), where.begin(), where.end());
        }
        else {
            std::string error_message = generate_error_message("Identifier '%' not recognised.");
            throw std::logic_error(debug::format_exception_message(error_message, param_right));
        }
    };
    //////////////////////////////////////////

    if (instruction.right_ptr_size == "BYTE") {
        handle(0x2B, 0x3B);
    }
    else if (instruction.right_ptr_size == "WORD") {
        handle(0x2C, 0x3C);
    }
    else if (instruction.right_ptr_size == "DWORD") {
        handle(0x2D, 0x3D);
    }
    else {
        std::string error_message = generate_error_message("Size % not recognised.");
        throw std::logic_error(debug::format_exception_message(error_message, instruction.right_ptr_size));
    }

    if (indirect) {
        machine_code.insert(machine_code.end(), {0x07, 0x12});  // + POP ESI
    }

    return machine_code;
}

std::vector<uint8_t> Assembler::handle_variable(uint8_t opcode,
                                                const std::string &variable_name,
                                                const std::string &destination) {
    std::vector<uint8_t> machine_code;
    machine_code.push_back(opcode);

    int variable = std::count_if(variables.begin(), variables.end(), [&](const Variable &v) {
        if (v.name == variable_name) return true;
        return false;
    });


    if (variable) {
        if (!destination.empty()) machine_code.push_back(transform_register_to_num(destination));

        for (size_t i = 0; i < 4; ++i) machine_code.push_back(0x00);
    }
    else {
        auto message = generate_error_message("Symbol % not recognised.");
        throw std::logic_error(debug::format_exception_message(message, variable_name));
    }

    return machine_code;
}

std::string Assembler::generate_error_message(const std::string &message) const {
    std::string error_line{};
    for (const auto &instruction : instructions) {
        if (instruction.occur_file == current_executed_file && instruction.occur_line == current_executed_line) {
            error_line = instruction.get_instruction();
        }
    }

    std::string underline{};
    underline.insert(0, error_line.length(), 94); //^

    std::string error_message = "\nERROR (" + current_executed_file + "): At line " +
                                std::to_string(this->current_executed_line) + ": " +
                                message + "\n\t" +
                                error_line + "\n\t" +
                                underline;

    return error_message;
}
