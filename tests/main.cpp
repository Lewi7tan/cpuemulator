//
// Created by Artur Twardzik on 1/5/2021.
//
#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN

#include <iostream>
#include "doctest.h"

#include "../core/src/ram.h"
#include "../core/src/cpu.h"
#include "../core/src/bus.h"
#include "../assembler/src/assembler.h"

/*
 * REQUIRE - this level will immediately quit the test case if the assert fails
 *   and will mark the test case as failed.
 *
 * CHECK - this level will mark the test case as failed if the assert fails
 *   but will continue with the test case.
 *
 * WARN - this level will only print a message if the assert fails
 *   but will not mark the test case as failed.
 */

void check_files(const std::string &first, const std::string &second) {
    std::fstream file_generated;
    std::fstream file_prepared;

    file_generated.open(first);
    file_prepared.open(second);

    std::string line_f_generated{};
    std::string line_f_prepared{};
    int line_counter{};

    while (std::getline(file_generated, line_f_generated) && std::getline(file_prepared, line_f_prepared)) {
        if (line_f_prepared != line_f_generated) {
            std::cout << "About line: " << line_counter << " MISMATCH\n\t";
            std::cout << "Expected: " << line_f_prepared << "\n\t";
            std::cout << "Got:      " << line_f_generated << "\n";
            CHECK(line_f_prepared == line_f_generated);
        }

        ++line_counter;
    }

    file_generated.close();
    file_prepared.close();
}

TEST_CASE ("RAM") {
    ram::RAM ram;
    ram.memory_size = 0xffff;
    ram.stack_head = 0xffff;

    SUBCASE("LOADING SOURCE CODE") {
        std::vector<uint8_t> instructions{0x00, 0x01, 0x02, 0x03, 0x04};
        ram.load_program(instructions);

        auto first = ram.get_value(0, 4);
        auto second = ram.get_value(4, 1);

        REQUIRE(first == 0x00010203);
        REQUIRE(second == 0x04);

        std::cout << "[LOADING SOURCE CODE] pass\n";
    }

    SUBCASE("CREATING VARIABLES") {
        ram.allocate_value(0xBABA, 4, 0xFFBAFA01);
        auto variable4 = ram.get_value(0xBABA, 4);
        REQUIRE(variable4 == 0XFFBAFA01);

        ram.allocate_value(0xBAFC, 2);
        auto variable2 = ram.get_value(0xBAFC, 2);
        REQUIRE(variable2 == 0x00);

        ram.allocate_value(0xBAFE, 1, 0xff);
        auto variable1 = ram.get_value(0xBAFE, 1);
        REQUIRE(variable1 == 0xff);

        CHECK_THROWS_AS(ram.get_value(0xBAFE, 3), std::out_of_range);

        std::cout << "[CREATING VARIABLES] pass\n";
    }

    SUBCASE("WRITING TO VARIABLES") {
        ram.allocate_value(0xBABA, 4);

        ram.assign_value(0xBABA, 4, 0xFAFA);
        auto variable = ram.get_value(0xBABA, 4);
        REQUIRE(variable == 0xFAFA);

        std::cout << "[WRITING TO VARIABLES] pass\n";
    }

    SUBCASE("OPERATING ON STACK") {
        auto esp = ram.stack_head;

        ram.push(0xF6F6F6F6);
        esp -= 4;
        ram.push(0xF3FEF4FF);
        esp -= 4;
        ram.push(0xFAAAAAAF);
        esp -=4;

        auto top = ram.pop();
        REQUIRE(top == 0xFAAAAAAF);

        esp = ram.memory_size;

        ram.check_stack(esp);
        CHECK_THROWS_WITH_AS(ram.pop(), "Could not pop from empty stack.", std::out_of_range);

        std::cout << "[OPERATING ON STACK] pass\n";
    }

    SUBCASE("DELETING VARIABLES") {
        ram.allocate_value(0xBABA, 2, 0xf7e6);
        ram.free(0xBABA, 2);

        CHECK_THROWS_AS(ram.get_value(0xBABA, 2), std::out_of_range);

        std::cout << "[DELETING VARIABLES] pass\n";
    }
}

TEST_CASE ("ASSEMBLER") {
    std::unique_ptr<Assembler> as;
    SUBCASE("INSTRUCTIONS COMPATIBILITY WITH OPCODES") {
        as = std::make_unique<Assembler>("assembler/assembly.a");
        as->run();

        check_files("assembler/assembly.ce", "assembler/machine_code.mc");

        std::cout << "[INSTRUCTIONS COMPATIBILITY WITH OPCODES] pass\n";
    }

    SUBCASE("VARIABLES ADDRESSING") {
        as = std::make_unique<Assembler>("assembler/variables_addressing/variables.a");
        as->run();

        check_files("assembler/variables_addressing/variables.ce",
                    "assembler/variables_addressing/machine_code.mc");

        std::cout << "[VARIABLES ADDRESSING] pass\n";
    }

    SUBCASE("INDIRECT ADDRESSING") {
        as = std::make_unique<Assembler>("assembler/indirect/main.a");
        as->run();

        check_files("assembler/indirect/main.ce",
                    "assembler/indirect/main.mc");

        std::cout << "[INDIRECT ADDRESSING] pass\n";
    }

    SUBCASE("MULTIPLE ASSEMBLY FILES") {
        as = std::make_unique<Assembler>("assembler/multiple_files/main.a");
        as->run();

        check_files("assembler/multiple_files/main.ce",
                    "assembler/multiple_files/machine_code.mc");

        std::cout << "[MULTIPLE ASSEMBLY FILES] pass\n";
    }
}