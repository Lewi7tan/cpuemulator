;;;;; ASSIGNMENT ;;;;;;;;;;;;;;
31 00 00 00 00 05   ; MOV R0, 5
31 01 00 00 00 03   ; MOV R1, 3
31 05 00 00 00 0F   ; MOV R5, 15

;;;; LOGIC OPERATIONS ;;;;;;;;;
37 00 00 00 00 03   ; XOR R0, 3
27 00 01            ; XOR R0, R1
09 00               ; NOT R0
38 00 00 00 00 03   ; AND R0, 3
39 00 00 00 00 0F   ; OR R0, 15

;;;; ARITHMETIC OPERATIONS ;;;;
33 00 00 00 00 06   ; MUL R0, 6
34 00 00 00 00 05   ; DIV R0, 5
35 00 00 00 00 07   ; ADD R0, 7
36 00 00 00 00 03   ; SUB R0, 3

;;;; ARITHMETIC OP. ON REGS ;;;
23 00 01            ; MUL R0, R1
24 00 05            ; DIV R0, R5
25 00 01            ; ADD R0, R1
26 00 05            ; SUB R0, R5